#!/usr/bin/env python
# coding: utf-8
#untamed Yeti

# # This Code Places Game Files In Thier Correct Region
import glob
import os
import shutil
from os.path import join

# # Function To Create Folders And Move Files to Proper Regions For NoIntro Sets

#Create directories if not exist and place files in correct directory
def nointro (directory):
    #removeing exsiting folders
    if os.path.exists(directory):
        print(directory + ' exists - no need to create a new folder')
    #creating new foldors
    if not os.path.exists(directory):
        os.makedirs(directory)
        print('created folder - ' + directory)
    #placing files in directory
    for f in glob.glob('*'):
        if '({})'.format(directory) in f:
            print('handling - {}'.format(f))
            try:
                shutil.move(f, directory)
            except:
                os.remove(f)
            print (f)
    print('Done with region {}'.format(directory))
    return


# # Game Regions for No Intro Sets
region_list = ['Demo','Demo 2','Demo 3', 'Demo 4', 'Demo 5','Sample','Sample 1','Sample 2','Sample 3','Sample 4','Sample 5','Proto','Proto 1','Proto 2','Proto 3','Proto 4','Proto 5','Beta','Beta 1','Beta 2','Beta 3','Beta 4','Beta 5','Promo','Unl','Australia','Brazil','Canada','China','Denmark','Europe','Germany','France','Italy','Japan','Korea','Netherlands','Spain','Sweden','Taiwan','USA','World','Europe, Australia','Europe, Brazil','Japan, Brazil','Japan, Europe, Brazil','Japan, Europe','Japan, Hong Kong','Japan, Korea','Japan, USA','Japan, USA, Brazil','USA, Australia','USA, Brazil','USA, Europe','USA, Europe, Brazil', 'Brazil, Portugal','USA, Europe, Korea','Asia','Mexico','USA, Korea','Japan, USA, Korea','Asia, Korea','USA, Asia, Korea','Europe, Asia','Europe, Korea','Hong Kong','Japan, Korea, Asia','USA, Asia','Japan, Asia', 'Finland','Europe, Canada', 'Japan, Asia, Korea']

# # Change To The Collection You Want To Work With
path = r'F:\Staging\No-Intro' # this will include subfolders
os.chdir(path)

#loop through list and run function
for region in region_list:
    for f in glob.glob('*'):
        if '({})'.format(region) in f:
            print('Discoverd - {}'.format(region))
            nointro(region) #if found - we are running the script to place the file
            break
print('Finished')


# # This function takes care of all folders in dir 

def nointro_folder (path):
    #loop through all NOINTRO FILES in FOLDER:
    os.chdir(path)
    for region in region_list:
        for f in glob.glob('*'):
            if '({})'.format(region) in f:
                print('Discoverd - {}'.format(region))
                nointro(region) #if found - we are running the script to place the file
                break
    return


files = os.listdir(path)
for name in files:
    newpath=path+'\\'+name
    #print(newpath)
    nointro_folder(newpath)
print('Finished')



